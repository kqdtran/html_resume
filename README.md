html_resume
======

My [web resume](http://kqdtran.github.com/html_resume/) on Github. A similar one, along with the pdf version, can be found [here](http://www.ocf.berkeley.edu/~kqdtran/resume.html). 